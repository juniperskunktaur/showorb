import { GluegunCommand, GluegunToolbox } from 'gluegun';

const command: GluegunCommand = {
  name: 'showorb',
  run: async (toolbox: GluegunToolbox) => {
    const {
      // parameters,
      template: { generate },
      filesystem,
      print: { info }
    } = toolbox;

    // get list of all files in images folder

    const IMAGE_FOLDER = filesystem.path('images');
    const imageTree = await filesystem.inspectTreeAsync(IMAGE_FOLDER, { relativePath: false });

    await generate({
      template: 'slideshow.html.ejs',
      target: `showorb-slideshow.html`,
      props: { 
        images: imageTree.children,
        revealOptions: {},
       }
    });

    info(`Generated slideshow at ./showorb-slideshow.html`);
  }

}

module.exports = command;
