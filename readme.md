# showorb

Takes a folder of images and generates a reveal.js slideshow on auto-rotate, for use by streamers in something like OBS.

Built with [Reveal.js](https://revealjs.com/) and [Gluegun](https://github.com/infinitered/gluegun).

## To install

* Install Node.JS (LTS is fine) - download from https://nodejs.org/
* install Yarn: `npm install --global yarn`
* Download source code for showorb from GitLab - [Download showorb-master.zip](https://gitlab.com/juniperskunktaur/showorb/-/archive/master/showorb-master.zip)
* Unzip folder anywhere
* From the command line, `cd` into the `showorb` folder
* Run `yarn link`
* Run `showorb` to see if it's working
  * if it can't find showorb, you may not have yarn's global `bin` folder in your path. If you just installed Yarn, try rebooting or logging out and in again. 
  * If rebooting doesn't work, running `yarn global bin` will show you the folder to add to your path. [Read more about yarn global](https://classic.yarnpkg.com/en/docs/cli/global/) and [what the path is](https://superuser.com/a/284351). Note changing your path in a file or in windows settings will mean you need to close and restart any terminal windows for it to take effect.

## To use

* Copy images into the `images` folder - anything a web browser could open: jpg, gif, png, webp, jp2k, etc. All files found will be treated as images. Remove any non-image files. There is no video support yet, but animated gifs work fine.
* From the command line, `cd` into the `showorb` folder
* Run `showorb` from the command line
* Use the generated `showorb-slideshow.html` as you see fit.

Out of the box, the slideshow is on shuffle, loop, and auto-advance every 5000 milliseconds. It will reshuffle every time you refresh the page.

## To customise

* Edit the `src/templates/slideshow.html.ejs` file as you see fit.
  * Use CSS to customise the background color or image
  * Edit the Reveal.js options to control how the slideshow runs
  * Read more about [Reveal.js](https://revealjs.com/).
* Run `showorb` from the `showorb` folder again.

## License

MIT - see LICENSE
