const { system, filesystem } = require('gluegun')

const src = filesystem.path(__dirname, '..')

const cli = async cmd =>
  system.run('node ' + filesystem.path(src, 'bin', 'showorb') + ` ${cmd}`)

test('outputs version', async () => {
  const output = await cli('--version')
  expect(output).toContain('0.0.1')
})

test('outputs help', async () => {
  const output = await cli('--help')
  expect(output).toContain('0.0.1')
})

test.skip('generates file', async () => {
  const output = await cli('')

  expect(output).toContain(`Generated slideshow at ./showorb-slideshow.html`)
  const foomodel = filesystem.read('showorb-slideshow.html')

  expect(foomodel).toContain(`<script src="./node_modules/reveal.js/dist/reveal.js"></script>`)

  // cleanup artifact
  filesystem.remove('showorb-slideshow.html')
})
